defmodule WordListTest do
  use ExUnit.Case

  alias Dictionary.WordList

  test "word_list returns a non empty list" do
    assert [_ | _] = WordList.word_list()
  end
  
  test "random_word returns a binary" do
    assert is_binary(WordList.random_word())
  end
end
