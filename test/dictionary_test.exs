defmodule DictionaryTest do
  use ExUnit.Case
  doctest Dictionary

  test "random_word returns a string (binary)" do
    assert is_binary(Dictionary.random_word())
  end
end
